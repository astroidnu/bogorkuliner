-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09 Agu 2015 pada 04.38
-- Versi Server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `api_makanan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_gambar`
--

CREATE TABLE IF NOT EXISTS `tb_gambar` (
  `id_tempat` int(11) NOT NULL,
`id_gambar` int(11) NOT NULL,
  `path_gambar` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_gambar`
--

INSERT INTO `tb_gambar` (`id_tempat`, `id_gambar`, `path_gambar`) VALUES
(1, 1, 'LG_1.jpg'),
(1, 2, 'LG_2.jpg'),
(2, 3, 'Venus1.jpg'),
(2, 4, 'Venus2.jpg'),
(2, 5, 'Venus3.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_tempat`
--

CREATE TABLE IF NOT EXISTS `tb_tempat` (
`id_tempat` int(11) NOT NULL,
  `nama_tempat` varchar(200) NOT NULL,
  `alamat` varchar(300) NOT NULL,
  `deskripsi` text NOT NULL,
  `latitude` float(20,8) NOT NULL,
  `long` float(20,8) NOT NULL,
  `no_telp` varchar(200) NOT NULL,
  `link_img` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_tempat`
--

INSERT INTO `tb_tempat` (`id_tempat`, `nama_tempat`, `alamat`, `deskripsi`, `latitude`, `long`, `no_telp`, `link_img`) VALUES
(1, 'Macaroni Panggang ', ' Jl. Salak No. 24, Jawa Barat 16128, Indonesia', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', -6.58849096, 106.80287170, '(0251) 8377519', 'lasagana_gulung'),
(2, 'Roti Unyil Venus Bogor', 'Jl. Pajajaran Komplek V Point No. 1 Bantarjati Bogor Utara - Kota Kota, Bogor, Jawa Barat 16119, Indonesia', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', -6.62087011, 106.81692505, '(0251) 8364008', 'roti_unyil');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_gambar`
--
ALTER TABLE `tb_gambar`
 ADD PRIMARY KEY (`id_gambar`), ADD UNIQUE KEY `id_gambar` (`id_gambar`);

--
-- Indexes for table `tb_tempat`
--
ALTER TABLE `tb_tempat`
 ADD PRIMARY KEY (`id_tempat`), ADD UNIQUE KEY `id_tempat` (`id_tempat`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_gambar`
--
ALTER TABLE `tb_gambar`
MODIFY `id_gambar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_tempat`
--
ALTER TABLE `tb_tempat`
MODIFY `id_tempat` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
