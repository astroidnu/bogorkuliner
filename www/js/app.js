// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app=angular.module('starter', ['ionic','ngCordova'])

.run(function($ionicPlatform,$ionicPopup) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
  $ionicPlatform.registerBackButtonAction(function (event) {
            //ionic.Platform.exitApp();
             $ionicPopup.confirm({
                title: "KELUAR APLIKASI",
                content: "Apakah Anda ingin keluar dari aplikasi?"
              })
              .then(function(result) {
              if(result) {
                ionic.Platform.exitApp();
                }
              });
                }, 100);
})




.config(function($stateProvider, $urlRouterProvider) {
  
  $stateProvider

  .state('MainMenu',{ 
    url:"/MainMenu",
    cache:false,
    templateUrl:"templates/MainMenu.html",
    controller:'MainCtrl'
  })

  .state('menu1',{
    url:"/menu1",
    cache:false,
    templateUrl:"templates/menu1.html",
    controller: 'MenuCtrl'
  })

  .state('menu',{
    url:"/menu",
    cache:false,
    templateUrl:"templates/menu.html",
    controller:'Menu2Ctrl'    
  })

  .state('about',{
    url:"/about",
    cache:false,
    templateUrl:"templates/about.html",
    controller:'AboutCtrl'
  })

  .state('menu.tab1',{
    url:"/tab1",
    views:{
      'deskripsi-tab':{
        cache:false,
      templateUrl:"templates/tab1.html",
      controller: 'DeskripsiCtrl'
    }
    }
  })

.state('menu.tab2',{
    url:"/tab2",
    views:{
      'galeri-tab':{
        cache:false,
      templateUrl:"templates/tab2.html",
      controller: 'GaleriCtrl'
    }
    }
  })

.state('menu.tab3',{
    url:"/tab3",
    views:{
      'map-tab':{
        cache:false,
      templateUrl:"templates/tab3.html",
      controller: 'MapCtrl'
    }
    }
  });

  $urlRouterProvider.otherwise('/MainMenu');
})



app.controller('MainCtrl', function($scope,$state,$cordovaGeolocation,PosisiLat,PosisiLong){
  var posOptions = {timeout: 10000, enableHighAccuracy: false};
   $cordovaGeolocation
    .getCurrentPosition(posOptions)
    .then(function (position) {
      var lat  = position.coords.latitude
      PosisiLat.addID(lat);
      var long = position.coords.longitude
      PosisiLong.addID(long);
      console.log(lat,long);  
    }, function(err) {
      // error
    });
  $scope.daftar=function(){
    $state.go('menu1');
  }
  $scope.about=function(){
    $state.go('about');
    PosisiLat.deleteIDList();
    PosisiLong.deleteIDList();
  }

})

app.controller('AboutCtrl', function($scope,$state,$ionicSlideBoxDelegate,PosisiLat,PosisiLong){
    
    $scope.kembali3= function(){
      $state.go('MainMenu');
          PosisiLat.deleteIDList();
    PosisiLong.deleteIDList();
    }

    $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  }

  })


app.controller('MenuCtrl', function($scope,$state,$http,IDMakanan,$ionicHistory,PosisiLong,PosisiLat){
    $scope.kembali1= function(){
      $ionicHistory.clearCache();
      $state.go('MainMenu',{reload:true});
          PosisiLat.deleteIDList();
    PosisiLong.deleteIDList();

    }

     $scope.menu3= function(id){
      $state.go('menu');
      IDMakanan.addID(id);
    }
   $http.get("http://192.168.0.14/data_makanan/tempat.php")
.success(function(data){
  $scope.tempat = data.api_makanan;
  console.log($scope.tempat);
  });
  
  
})







app.controller('Menu2Ctrl', function($scope,$state,$ionicHistory,IDMakanan){
    $scope.kembali2= function(){
      $ionicHistory.clearCache();
      IDMakanan.deleteIDList();
      $state.go('menu1');
    }
  })





app.controller('DeskripsiCtrl', function($scope, $http, IDMakanan){
  var idbaru = IDMakanan.getID();
  $http.get("http://192.168.0.14/data_makanan/?id="+ idbaru)
  .success(function(data){
  $scope.deskripsi2 = data.api_makanan[0].deskripsi;
  $scope.alamat = data.api_makanan[0].address;
  $scope.notelp = data.api_makanan[0].telp;

  });

})


app.controller('GaleriCtrl', function($scope, $http ,$ionicModal,IDMakanan) {

 var idbaru = IDMakanan.getID();
    $http.get("http://192.168.0.14/data_makanan/?id="+ idbaru)
.success(function(data){
  $scope.gambar = data.api_makanan;
  console.log($scope.gambar);
  });

 
 $scope.showImages = function(index) {
 $scope.activeSlide = index;
 $scope.showModal('templates/image-popover.html');
 }
 
 $scope.showModal = function(templateUrl) {
 $ionicModal.fromTemplateUrl(templateUrl, {
 scope: $scope,
 animation: 'slide-in-up'
 }).then(function(modal) {
 $scope.modal = modal;
 $scope.modal.show();
 });
 }
 
 // Close the modal
 $scope.closeModal = function() {
 $scope.modal.hide();
 $scope.modal.remove()
 };

})




app.controller('MapCtrl', function($scope,IDMakanan,$http,PosisiLat,PosisiLong) {
console.log(PosisiLat.getID());
console.log(PosisiLong.getID());
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;
$scope.initialize = function() {
    calcRoute();
  directionsDisplay = new google.maps.DirectionsRenderer();
  var bogor = new google.maps.LatLng(-6.597410, 106.802878);
  var mapOptions = {
    zoom:10,
    center: bogor
  };
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
  directionsDisplay.setMap(map);
    $scope.map = map;                    
}

function calcRoute() {
 var idbaru = IDMakanan.getID();
$http.get("http://192.168.0.14/data_makanan/?id="+ idbaru)
.success(function(data){
   var latitude = data.api_makanan[0].lat;
  var longitude = data.api_makanan[0].long;
  var lat = PosisiLat.getID();
  var long2 = PosisiLong.getID();
  var start = lat +','+ long2;
 var end = latitude+','+longitude;
  var request = {
      origin:start,
      destination:end,
      travelMode: google.maps.TravelMode.DRIVING
  };

  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
  });
   
}
})


.service('PosisiLat',function(){
  var PosisiListLat= [];

  var addID= function(newObj) {
      PosisiListLat.push(newObj);
  }

  var getID = function(){
      return PosisiListLat;
  }

  var deleteIDList = function(){
      PosisiListLat = [];  
  }

  return {
    addID: addID,
    deleteIDList:deleteIDList,
    getID:getID
  };
})

.service('PosisiLong',function(){
  var PosisiListLong= [];

  var addID= function(newObj) {
      PosisiListLong.push(newObj);
  }

  var getID = function(){
      return PosisiListLong;
  }

  var deleteIDList = function(){
      PosisiListLong = [];  
  }

  return {
    addID: addID,
    deleteIDList:deleteIDList,
    getID:getID
  };
})




.service('IDMakanan',function(){
  var IDMakananList= [];

  var addID= function(newObj) {
      IDMakananList.push(newObj);
  }

  var getID = function(){
      return IDMakananList;
  }

  var deleteIDList = function(){
      IDMakananList = [];  
  }

  return {
    addID: addID,
    deleteIDList:deleteIDList,
    getID:getID
  };
})


